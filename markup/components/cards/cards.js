let cardsList = {
    config: {
        'block': '.cards',
        'template': '.cards__template',
        'lists': '.cards__list',
        'item': '.cards__item'
    },

    add: function (data) {
        let $self = this,
            $block = $($self.config.block),
            $template = document.querySelector($self.config.template).content.querySelector($self.config.item),
            $list = $block.find($self.config.lists);

        for (let item of data) {
            let elem = $($template).find('[data-name="' + item.name + '"]');

            if (elem.length > 0) {
                elem.text(item.value);
            }
        }

        $($list).append($($template).parent().html());
    }
};
