/* global cardsList */

let formFeedback = {
    config: {
        'form': '.form'
    },

    init: function () {
        const $self = this;
        $($self.config.form).on('submit', function (e) {
            e.preventDefault();
            $self.submitForm(this);
        });
    },

    submitForm: function (form) {
        const $self = this;
        if ($self.formValid(form)) {
            $self.sendForm(form);
        }
    },

    formValid: function (form) {
        let $form = $(form),
            $data = $form.serializeArray(),
            $steps = 0,
            $totalSteps = 0;

        for (let $item of $data) {
            let $input = $form.find('[name="' + $item.name + '"]'),
                $pattern = $input.data('pattern');
            $totalSteps++;

            $input.removeClass('input_state_error');

            switch ($pattern) {

                case 'email': {
                    const $regexp = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

                    if ($regexp.test($input.val())) {
                        $steps++;
                    } else {
                        $input.addClass('input_state_error');
                        $input.focus();
                    }
                    break;
                }

                case 'textlen': {
                    if ($input.val().length > 1) {
                        $steps++;
                    } else {
                        $input.addClass('input_state_error');
                        $input.focus();
                    }
                    break;
                }

                default: {
                    $steps++;
                    break;
                }
            }
        }

        if ($steps === $totalSteps) {
            return true;
        } else {
            return false;
        }
    },

    sendForm: function (form) {
        let $form = $(form),
            $sendData = $form.serialize(),
            $url = $form.attr('action');

        $.post($url, $sendData, function (data) {
            let status = data.status;
            if (status) {
                $('.message-insert').addClass('message-insert_state_success');

                if (cardsList) {
                    cardsList.add($form.serializeArray());
                    $form.find('input, textarea').val(null);
                }
            } else {
                $('.message-insert').removeClass('message-insert_state_success');
            }
        }, 'json');
    }
};

formFeedback.init();
